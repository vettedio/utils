package microservice

import (
	"github.com/beevik/guid"
)

type Request struct {
	TID       string
	Headers   map[string]interface{}
	Route     string
	Target    string
	Params    map[string]interface{}
	Token     *Token
	Method    string
	Raw       []byte
}

//TODO: Refactor AuthToken into the Request Header
func (req *Request) getToken() Token {
	return *req.Token
}

func  GenerateTID() string {
	return guid.New().String()
}

type Response struct {
	TID     string			         `json:"tid"`
	Headers map[string]interface{}   `json:"headers"`
	Result  interface{}	             `json:"result"`
	Error   *ErrResponse		     `json:"error"`
}

type ErrResponse struct {
	ErrorCode int
	Message   string
	Error     error
}