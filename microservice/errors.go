package microservice

import (
	"errors"
	"net/http"
)

func InvalidParameterException(message string)(response Response){
	 return Response{
		 Error:   &ErrResponse{
			 ErrorCode: http.StatusInternalServerError,
			 Message:   message,
			 Error:     errors.New("InvalidParameterException"),
		 },
	 }
}

func ValidationException(message string)(response Response){
	return Response{
		Error:   &ErrResponse{
			ErrorCode: http.StatusBadRequest,
			Message:   message,
			Error:     errors.New("ValidationException"),
		},
	}
}

func UnauthorizedRequestException()(response Response){
	return Response{
		Error: &ErrResponse{
			ErrorCode: http.StatusUnauthorized,
			Message: "Unauthorized Request",
			Error: errors.New("UnauthorizedException"),
		},
	}
}

func UserSessionExpiredException()(response Response){
	return Response{
		Error: &ErrResponse{
			ErrorCode: http.StatusUnauthorized,
			Message: "Unauthorized Request",
			Error: errors.New("UnauthorizedException"),
		},
	}
}